// ==UserScript==
// @name         Bitbucket Diff total
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  Displays the total changes on any bitbucket page with a diff inside
// @author       Merlin, Potti, BP2017D2
// @match        https://bitbucket.org/*
// @grant        none
// ==/UserScript==

(function () {
    'use strict';

    window.onload = (function () {
        const tryDiff = function () {
            createDiffDisplay();
            setTimeout(tryDiff, 250);
        };
        tryDiff();
    });
})();

function createDiffDisplay() {
    const diffRoot = document.getElementById("commit-files-summary");
    if (diffRoot === null) return false;

    const oldListElement = document.getElementById("total-diff-li");
    if (oldListElement !== null) return false;

    const listElement = document.createElement("li");
    listElement.id = "total-diff-li";
    const diffs = getTotalDiff();

    const totalChangesString = (diffs.additions + diffs.deletions) + ' / ' + (diffs.additions - diffs.deletions);
    let newHTML =
        '<div class="commit-file-diff-stats">' +
            '<span class="lines-added">+' + diffs.additions + '</span>' +
            '<span class="lines-removed">' + diffs.deletions + '\n</span>' +
        '</div>' +
        '<b>' +
            'Total Changes (' + totalChangesString + ')' +
        '</b>';
    if (diffs.incomplete) {
        newHTML += "  (incomplete, <a onclick='calculateFullDiff()'>load and calculate full diff</a>)";
    }
    listElement.innerHTML = newHTML;
    diffRoot.insertBefore(listElement, diffRoot.firstChild);
    return true;
}

function getTotalDiff() {
    const diffListRoot = document.getElementById('commit-files-summary');
    const additions = getInnerTextSum(diffListRoot, 'lines-added');
    const deletions = getInnerTextSum(diffListRoot, 'lines-removed');
    return {
        additions: additions.sum,
        deletions: deletions.sum,
        incomplete: additions.incomplete || deletions.incomplete
    };
}

function getInnerTextSum(rootElement, className) {
    let sum = 0;
    let incomplete = false;
    const elementList = rootElement.getElementsByClassName(className);
    for (let i = 0; i < elementList.length; i++) {
        const currentAddition = elementList[i];
        const thisNumber = parseInt(currentAddition.innerText);
        if (!isNaN(thisNumber)) {
            sum += thisNumber;
        } else {
            incomplete = true;
        }
    }
    return {
        sum: sum,
        incomplete: incomplete
    }
}

window.calculateFullDiff = function() {
    $(".load-diff.try-again").click();
    const old = document.getElementById("total-diff-li");
    if (old != undefined) {
        old.parentNode.removeChild(old);
    }
};