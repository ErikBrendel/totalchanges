# TotalChanges - Script

This script enables your browser to show the total diffs of any diff list on Bitbucket


## Usage

 - Install the [Tampermonkey extension](http://tampermonkey.net/) for your browser
 - Copy the content of script.js to a new tampermonkey-script